import { InterfaceMiddleware } from '@isolutions/distributor-lib';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as debug from 'debug';
import * as morgan from 'morgan';

export const networkManager: InterfaceMiddleware = {
    handle: (app) => {
        app.set('x-powered-by', false);
        if (app.get('ENV') !== 'production') {
            app.use((req, res, next) => {
                res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
                res.header('Expires', '-1');
                res.header('Pragma', 'no-cache');
                next();
            });
        }
    },
};

export const middlewareManager: InterfaceMiddleware = {
    handle: (app) => {
        const accessLog = debug('app-access-log');
        app.use(morgan('tiny', { stream: { write: (msg) => accessLog(msg) } }));
        app.use(bodyParser.json());
        app.use(compression());
    },
};

export const errorHandler: InterfaceMiddleware = {
    handle: (app) => {
        app.use((err, req, res, next) => {
            const code = err.code || 401;
            const message = (app.get('ENV') === 'production') ? err : err.message;
            res.status(code);
            res.json(message);

            next(err);
        });
    },
};
