import * as debug from 'debug';
import RoutingError from '../../common/errors/routing-error';
import {
    LIST_ALL_ROUTE, LIST_AVAILABLE_ROUTE, LOCK_VEHICLE_ROUTE, NOT_FOUND, UNLOCK_VEHICLE_ROUTE,
    VIEW_VEHICLE_ROUTE,
} from '../../common/routes';

const listAll = (options) => {
    return {
        callback: async (req, res, next) => {
            try {
                const { collection } = options;
                const result = await collection.getAll();
                res.status(200).json(result);
            } catch (e) {
                next(e);
            }
        },
        method: 'get',
        params: [],
        path: LIST_ALL_ROUTE,
    };
};

const listAvailable = (options) => {
    return {
        callback: async (req, res, next) => {
            try {
                const { collection } = options;
                const vehicles = await collection.getAvailable((vehicle) =>  vehicle.status === 'free');
                res.status(200).json(vehicles);
            } catch (e) {
                next(e);
            }
        },
        method: 'get',
        params: [],
        path: LIST_AVAILABLE_ROUTE,
    };
};

const viewVehicleById = (options) => {
    return {
        callback: async (req, res, next) => {
            try {
                const { collection } = options;
                const id = parseInt(req.params.id, 10);
                const result = await collection.getById(id);
                res.status(200).json(result);
            } catch (e) {
                next(e);
            }
        },
        method: 'get',
        params: [],
        path: VIEW_VEHICLE_ROUTE,
    };
};

const lockVehicleForProcessing = (options) => {
    return {
        callback: async (req, res, next) => {
            try {
                const { collection } = options;
                const id = parseInt(req.params.id, 10);
                await collection.lock(id);
                res.status(200).json('ok');
            } catch (e) {
                next(e);
            }
        },
        method: 'post',
        params: [],
        path: LOCK_VEHICLE_ROUTE,
    };
};

const unlockVehicleAfterProcessing = (options) => {
    return {
        callback: async (req, res, next) => {
            try {
                const { collection } = options;
                const id = parseInt(req.params.id, 10);
                await collection.unlock(id);
                res.status(200).json('ok');
            } catch (e) {
                next(e);
            }
        },
        method: 'post',
        params: [],
        path: UNLOCK_VEHICLE_ROUTE,
    };
};

const notFound = () => {
    return {
        callback: async (req, res, next) => {
           next(new RoutingError(`Sorry, no such page here. Url: ${req.url}`, 404));
        },
        method: 'all',
        params: [],
        path: NOT_FOUND,
    };
};

export default (options) => [
    listAll(options),
    listAvailable(options),
    viewVehicleById(options),
    lockVehicleForProcessing(options),
    unlockVehicleAfterProcessing(options),
    notFound(),
];
