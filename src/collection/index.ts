import { Collection, Server, util } from '@isolutions/distributor-lib';
import * as debug from 'debug';
import { InterfaceVehicle } from '../common/entity/';
import Config from './config';
import { errorHandler, middlewareManager, networkManager } from './infra';
import routes from './infra/routes';
import LocationMutator from './mutators/vehicle-location';
import Taxi5Client from './services/taxi5-client';

(async () => {
    const log: debug.IDebugger = debug('app-bootstrap');
    try {
        const config = new Config();
        log('Configuring app. Config: %o', config);

        const { vehicleSyncClient, vehicleCollection } = config;
        const client = new Taxi5Client<InterfaceVehicle>({ ...vehicleSyncClient });
        const collection = new Collection<InterfaceVehicle>({
            client,
            stateLifespan: vehicleCollection.stateLifespan,
        });

        const vehicleLocationMutator = new LocationMutator(config.vehicleLocation);
        await util.connect(() => vehicleLocationMutator.connect(), 5);
        collection.attachMutator(vehicleLocationMutator);
        log('Location mutator attached');

        await util.connect(() => collection.load(), 5);
        log('Initial loading of data performed');

        collection.watch(vehicleCollection.updateInterval);
        log('Collection watch set to %o seconds', vehicleCollection.updateInterval);

        const server = new Server();

        server.attachMiddleware(middlewareManager);
        server.attachMiddleware(networkManager);
        server.attachMiddleware(errorHandler);

        const options = {
            collection,
        };

        routes(options).forEach((route) =>  server.attachRoute(route));

        const app = server.run(config.express.port, config.express.host);
        log('Running app');

        process.on('SIGTERM', () => {
            app.close(() => {
                // TODO do we even need this handler?
            });

            setTimeout(() => {
                log('Could not exit normally. Forcefull shitting down');
                process.exit(1);
            }, 30 * 1000);

        });
    } catch (error) {
        log('Bootstrap failed: %o', error);
        return false;
    }
})();
