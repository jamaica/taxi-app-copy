import { InterfaceMutator } from '@isolutions/distributor-lib';
import { SocketIOMessage } from '@taxi5/tracking';
import * as debug from 'debug';
import * as Client from 'socket.io-client';
import Timer = NodeJS.Timer;

import { InterfaceLocation, InterfaceVehicle } from '../../common/entity';

interface InterfaceStateEntity {
    location: InterfaceLocation;
    timestamp: number;
}

export default class VehicleLocation implements InterfaceMutator<InterfaceVehicle> {
    private locationTimeout: number;
    private pruneTimeout: number;
    private state: Map<string, InterfaceStateEntity>;
    private client: Client;
    private log: debug.IDebugger;
    private interval: Timer;

    public constructor(options) {
        const { url, locationTimeout, pruneTimeout } = options;

        this.locationTimeout = locationTimeout;
        this.pruneTimeout = pruneTimeout;
        this.client = new Client(url);
        this.state = new Map();
        this.log = debug('app-vehicle-location-mutator');
    }

    public setClient(client: Client) {
        this.client = client;
    }

    public async connect(): Promise<boolean> {
        try {
            this.client.on('open', async () => this.log('connected'));
            this.client.on('location', async (message: SocketIOMessage<string, InterfaceLocation>) => {
                const { entity, data, timestamp } = message;

                this.state.set(entity, { location: data, timestamp });
            });

            this.client.on('close', async () => this.log('Closed'));
            this.interval = setInterval(() => {
                this.log('Purging outdated locations');
                for (const [key, entry ] of this.state) {
                    if (Date.now() - new Date(entry.timestamp).getMilliseconds() > 1000 * this.locationTimeout) {
                        this.log('deleting key %o', key);
                        this.state.delete(key);
                    }
                }
            }, 1000 * this.pruneTimeout);

            return true;
        } catch (error) {
            this.log('Connection error. Extra: %o', error);
            return false;
        }
    }

    public async disconnect(): Promise<boolean> {
        clearInterval(this.interval);
        const { readyState } = this.client;

        switch (readyState) {
            case 0:
                await new Promise((resolve) => {
                    this.client.on('open', async () => resolve());
                });
                break;
            case 2:
            case 3:
                return true;
        }
        this.client.close();
    }

    public async handle(state: InterfaceVehicle[]): Promise<InterfaceVehicle[]> {
        return state.map((vehicle: InterfaceVehicle): InterfaceVehicle => {
            if (this.state.has(vehicle.controller_id)) {
                const location = this.state.get(vehicle.controller_id).location;
                this.log('Location updated for controller_id %o: ', vehicle.controller_id);

                return {...vehicle, location};
            }
            this.log('Updated location not available for controller_id %o: ', vehicle.controller_id);

            return vehicle;
        });
    }
}
