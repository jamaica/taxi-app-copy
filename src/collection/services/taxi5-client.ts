import * as debug from 'debug';
import { IDebugger } from 'debug';
import * as transport from 'request-promise-native';

export interface InterfaceTaxi5Client<T> {
    getEntities: () => Promise<T[]>;
}

export default class Taxi5Client<T> implements InterfaceTaxi5Client<T> {
    private url;
    private request;
    private log: IDebugger;

    constructor(options) {
        this.log = debug('app-taxi5-client');
        const { url } = options;
        this.url = url;
        /*this.params = {
            ...this.params,
            key: token,
        };*/
        this.request = transport.defaults({
            json: true,
        });

        this.log('Initialized');
    }

    public async getEntities(): Promise<T[]> {
        try {
            return this.request.get(this.url);
        } catch (e) {
            this.log('Request resulted in error: %O', e);
            throw e;
        }
    }
}
