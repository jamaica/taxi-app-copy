export interface InterfaceConfiguration {
    express: {
        'host': string,
        'port': number,
    };

    vehicleSyncClient: {
        url: string,
    };

    vehicleCollection: {
        updateInterval: number,
        stateLifespan: number,
    };

    vehicleLocation: {
        url: string,
        locationTimeout: number,
        pruneTimeout: number,
    };
}

export default class Config implements InterfaceConfiguration {
    public express = {
        host: process.env.EXPRESS_HOSTNAME || 'localhost',
        port: parseInt(process.env.EXPRESS_PORT, 10) || 3000,
    };

    public vehicleSyncClient = {
        url: process.env.SYNC_URL || 'http://sync.taxi5.by:8081/vehicles?key=token',
    };

    public vehicleCollection = {
        stateLifespan: parseInt(process.env.STATE_LIFESPAN, 10) || 15 * 60,
        updateInterval: parseInt(process.env.UPDATE_INTERVAL, 10) || 4,
    };

    public vehicleLocation = {
        locationTimeout: parseInt(process.env.TRACKER_LOCATION_TIMEOUT, 10) || 60 * 5,
        pruneTimeout: parseInt(process.env.TRACKER_LOCATION_TIMEOUT, 10) || 60 * 5,
        url: process.env.TRACKER_URL || 'http://37.1.207.77:8082',
    };
}
