export interface InterfaceConfiguration {
    rabbitmq: {
        prefetch: number,
        queue: string,
        url: string,
        assertQueue: string,
    };

    osrm: {
        url: string;
        port: number;
    };

    collection: {
        hostname: string;
        port: number;
    };

    storage: {
        host: string,
        user: string,
        password: string,
        database: string,
        table: string,
    };

    confirmation: {
        port: number,
        retryCount: number,
        retryInterval: number,
        ackTimeout: number,
    };
}

export default class Config implements InterfaceConfiguration {
    public rabbitmq = {
        assertQueue: process.env.ASSERT_QUEUE || '{ durable: true }',
        prefetch: parseInt(process.env.RABBITMQ_PREFETCH, 10) || 10,
        queue: process.env.RABBITMQ_QUEUE_NAME || 'orders',
        url: process.env.RABBITMQ_URL || 'amqp://localhost',
    };

    public osrm = {
        port: parseInt(process.env.OSRM_PORT, 10) || 5000,
        url: process.env.OSRM_HOSTNAME || 'http://localhost',
    };

    public collection = {
        hostname: process.env.VEHICLE_COLLECTION_HOSTNAME || 'http://localhost',
        port: parseInt(process.env.VEHICLE_COLLECTION_PORT, 10) || 3000,
    };

    public storage = {
        database: process.env.MYSQL_DATABASE || 'orders',
        host: process.env.MYSQL_HOST || 'localhost',
        password: process.env.MYSQL_PASSWORD || 'password',
        port: parseInt(process.env.MYSQL_PORT, 10) || 3306,
        table: process.env.MYSQL_DECISION_RESULT_TABLE || 'decision_result',
        user:  process.env.MYSQL_USER || 'distributor',
    };

    public confirmation = {
        ackTimeout: parseInt(process.env.DRIVER_CONFIRMATION_SOCKET_CONNECTION_ACK_TIMEOUT, 10) || 10,
        port: parseInt(process.env.DRIVER_CONFIRMATION_PORT, 10) || 8080,
        retryCount: parseInt(process.env.DRIVER_CONFIRMATION_SOCKET_CONNECTION_RETRY_COUNT, 10) || 20,
        retryInterval: parseInt(process.env.DRIVER_CONFIRMATION_SOCKET_CONNECTION_RETRY_INTERVAL, 10) || 0.1,
    };
}
