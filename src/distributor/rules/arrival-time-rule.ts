import { Rule, RuleReport } from '@isolutions/distributor-lib';
import * as debug from 'debug';
import { InterfaceOrder, InterfaceVehicle } from '../../common/entity/index';

export default class ArrivalTimeRule implements Rule<InterfaceVehicle, InterfaceOrder, RuleReport> {
    private distancePriority = 1;
    private timePriority = 1;

    private osrm;

    constructor(osrm) {
        this.osrm = osrm;
    }

    public async rank(vehicle: InterfaceVehicle, order: InterfaceOrder): Promise<RuleReport> {
        const log = debug(`app-${order.id}-${vehicle.id}-arrival`);
        try {
            const from = [vehicle.location.longitude, vehicle.location.latitude];
            const to = [order.from.longitude, order.from.latitude];

            const { routes } = await this.osrm.route([from, to]);

            let { distance, duration } = routes[0];

            duration = (100 - duration / 60) * this.timePriority;
            distance = (100 - distance / 1000) * this.distancePriority;
            log('Distance: %o km, Duration: %o min', distance, duration);

            const weight: number = Math.round(+(((duration + distance) / 2).toFixed(2)));

            return { weight };

        } catch (e) {
            log('%o', e);
        }
    }
}
