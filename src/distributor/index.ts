import { Distributor, InterfaceCollectionEntityAPI, util } from '@isolutions/distributor-lib';
import { Client } from '@isolutions/osrm-client';
import * as amqp from 'amqplib';
import * as debug from 'debug';
import { InterfaceVehicle } from '../common/entity/index';
import Config from './config';
import AMPQ from './connectors/AMPQ';
import ArrivalTimeRule from './rules/arrival-time-rule';
import DistributorLibClient from './services/distributor-lib-client';
import DriverConfirmation from './services/driver-confirmation';
import { Validator } from './services/order-validator';
import Storage from './services/storage';
import filter from './services/vehicle-filter';

const log = debug('app-bootstrap');

(async () => {
    try {
        const config = new Config();
        log('Configuring app. Config: %o', config);

        const distributor = new Distributor();

        const AMPQConnector = new AMPQ(amqp, config.rabbitmq);
        await util.connect(() => AMPQConnector.connect(), 5);
        distributor.attachQueueConnection(AMPQConnector);
        log('AMQP connection initialized');

        const storage = new Storage(config.storage);
        await util.connect(() => storage.connect(), 5);
        distributor.attachStorage(storage);
        log('Mysql server connection initiqalized');

        distributor.attachOrderValidator(new Validator());

        distributor.attachStatePreProcessor(filter);
        const entityCollectionClient: InterfaceCollectionEntityAPI<InterfaceVehicle> =
            new DistributorLibClient(config.collection);
        distributor.attachDataSource(entityCollectionClient);
        log('Data source initialized');

        const osrm = new Client(`${config.osrm.url}:${config.osrm.port}`);
        const arrivalTimeRule = new ArrivalTimeRule(osrm);
        distributor.attachDecisionRule(arrivalTimeRule);
        log('Decision rules initialized');

        const driverConfirmation = new DriverConfirmation(config.confirmation);
        await driverConfirmation.connect();
        distributor.attachDecisionResultProcesser(driverConfirmation);
        log('Result processor initialized');

        await distributor.connect();
        log('Bootstrap completed');
    } catch (error) {
        log('Bootstrap failed: %O', error);
        return false;
    }
})();
