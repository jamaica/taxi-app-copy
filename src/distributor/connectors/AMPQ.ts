import { InterfaceConnector } from '@isolutions/distributor-lib';
import { Channel } from 'amqplib';
import * as debug from 'debug';
import { InterfaceOrder } from '../../common/entity/';

export interface InterfaceAMPQOptions {
    assertQueue: string;
    prefetch: number;
    queue: string;
    url: string;
}

export default class AMPQ implements InterfaceConnector {
    private config: InterfaceAMPQOptions;
    private channel: Channel;
    private log: debug.IDebugger;
    private transport;

    public constructor(transport: AMPQ, config: InterfaceAMPQOptions) {
        this.config = config;
        this.transport = transport;
        this.log = debug('app-ampq-connector');
    }

    public async connect() {
        try {
            this.log(`Connecting to ${this.config.url}`);
            const connection = await this.transport.connect(this.config.url);
            this.channel = await connection.createChannel();
            this.channel.assertQueue(this.config.queue, this.config.assertQueue);
            this.channel.prefetch(this.config.prefetch);

            return true;
        } catch (error) {
            this.log('Error connecting to ampq. Error: %o', error);
            return false;
        }
    }

    public async bind(handlers) {
        try {
            this.log('Attaching handler');
            this.channel.consume(this.config.queue, this.callback.bind(this, handlers), { noAck: false });
        } catch (error) {
            this.log('Queue initialization error. Error: %o', error);
            throw error;
        }
        return true;
    }

    private async callback(handlers, msg) {
        try {
            const order: InterfaceOrder = JSON.parse(msg.content.toString());
            this.log('Received order: %o', order);
            handlers.forEach((handler) => handler(order));
            this.channel.ack(msg);
        } catch (err) {
            this.log('Error processing order. Error: %O', err);
            let requeue = true;
            if (err.name === 'SyntaxError' || msg.fields.deliveryTag > 9) {
                requeue = false;
            }
            this.channel.nack(msg, false, requeue);
        }
    }
}
