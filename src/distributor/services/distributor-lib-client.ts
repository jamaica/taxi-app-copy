import { InterfaceCollectionEntityAPI } from '@isolutions/distributor-lib';
import * as debug from 'debug';
import * as transport from 'request-promise-native';
import { InterfaceVehicle } from '../../common/entity';
import {
    LIST_AVAILABLE_ROUTE, LOCK_VEHICLE_ROUTE, UNLOCK_VEHICLE_ROUTE, VIEW_VEHICLE_ROUTE,
} from '../../common/routes';

export default class DistributorLibClient implements InterfaceCollectionEntityAPI<InterfaceVehicle> {
    private request;
    private params = {};
    private log: debug.IDebugger;
    private endpoint: string;

    constructor(options) {
        this.log = debug('app-distributor-lib-client');
        const { hostname, port } = options;
        this.endpoint = `${hostname}:${port}`;
        this.request = transport.defaults({
            json: true,
        });
        this.log('Initialized');
    }

    public async getById(id): Promise<InterfaceVehicle> {
        const url = this.endpoint + VIEW_VEHICLE_ROUTE.replace(':id', id);

        return this.request.get(url, { qs: this.params });
    }

    public async getAvailable(): Promise<InterfaceVehicle[]> {
        const url = this.endpoint + LIST_AVAILABLE_ROUTE;

        return this.request.get(url, { qs: this.params });
    }

    public async lock(id): Promise<void> {
        const url = this.endpoint + LOCK_VEHICLE_ROUTE.replace(':id', id);

        return this.request.post(url, { qs: this.params });
    }

    public async unlock(id): Promise<void> {
        const url = this.endpoint + UNLOCK_VEHICLE_ROUTE.replace(':id', id);

        return this.request.post(url, { qs: this.params });
    }
}
