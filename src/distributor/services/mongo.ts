import { InterfaceStorage } from '@isolutions/distributor-lib';
import * as debug from 'debug';
import { MongoClient } from 'mongodb';

export default class Mongo implements InterfaceStorage {
    private log;
    private url;
    private db;

    public constructor(config) {
        this.log = debug('app-mongo-storage');
        const { host, port, username, password, project } = config;
        this.url = `mongodb://${username}:${password}@${host}:${[port]}/${project}`;
    }

    public async connect(): Promise<boolean> {
        this.log('Initializing server connection. Url %o', this.url);
        try {
            const client = await MongoClient.connect(this.url);
            this.db = client.db('logs');

            return true;
        } catch (error) {
            this.log(`Error. Message: %o. Extra: %o`, error.message, error.extra);
            return false;
        }
    }

    public async store(action, payload) {
        switch (action) {
            case 'handler-state' :
                this.db.collection('vehicles').insertOne(payload);
                break;

            case 'handler-order-received':
                this.db.collection('order').insertOne(payload);
                break;

            case 'handler-drivers-matched':
                this.db.collection('matched').insertOne(payload);
                break;

            case 'handler-driver-confirmed':
                this.db.collection('confirmed').insertOne(payload);
                break;

            case 'distributor-rules':
                this.db.collection('rules').insertOne(payload);
                break;
        }

        return true;
    }
}
