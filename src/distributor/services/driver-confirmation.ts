import { InterfaceOrderEntity, RankReport } from '@isolutions/distributor-lib';
import { retry } from 'async';
import * as debug from 'debug';
import * as express from 'express';
import { createServer } from 'http';
import * as io from 'socket.io';

export interface InterfaceDriverConfirmation {
    process(report: any, orderEntity: InterfaceOrderEntity): Promise<boolean>;
}

export default class DriverConfirmation implements InterfaceDriverConfirmation {
    private io;
    private log;
    private sockets = {};
    private options;

    public constructor(options) {
        this.log = debug('app-driver-confirmation');
        this.options = options;
    }

    public async connect() {
        const app = express();
        const server = createServer(app);

        const { port } = this.options;
        server.listen(port, () => {
            this.log('Listening for sockets on port %o', port);
        });
        this.io = io(server);
        this.io.on('connect', (socket: any) => {
            socket.on('init', (controllerId: string) => {
                this.log('ControllerId received: %o', controllerId);
                this.sockets = { ...this.sockets, [controllerId]: socket };
            });

            socket.on('error', (error) => {
                this.log('Socket error: %o', error);
            });
        });
    }

    public async process(report: any, orderEntity: InterfaceOrderEntity): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const collectionEntityId = report.id;
            const log = debug(`app-driver-confirmation-${orderEntity.id}-${collectionEntityId}`);
            const { retryCount, retryInterval, ackTimeout } = this.options;
            log('Sockets connected: %o', Object.keys(this.sockets));

            retry({ times: retryCount, interval: retryInterval * 1000 }, (callback) => {
                if (!this.sockets[collectionEntityId]) {
                    log('Socket %o not connected', collectionEntityId);
                    callback(true);
                    return;
                }

                setTimeout(() => {
                    reject(new Error('Confirmation time timeout.'));
                }, ackTimeout * 1000);

                this.sockets[collectionEntityId].emit('order', orderEntity, (response: boolean) => {
                    log('Vehicle %o response: %o', collectionEntityId, response);
                    (response) ? callback(null, response) : callback(true);
                });
            }, (err, result) => {
                // (err) ? reject(new Error(err)) : resolve(result);
            });
        });
    }
}
