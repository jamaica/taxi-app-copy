import { InterfaceOrderValidator } from '@isolutions/distributor-lib';
import * as debug from 'debug';
import * as validator from 'validator';
import { InterfaceOrder } from '../../common/entity';

export class Validator implements InterfaceOrderValidator {

    public async validate(order: InterfaceOrder): Promise<boolean> {
        const log = debug(`app-${order.id}-validate-order`);
        log('Validating order: %o', order);
        if (!order) {
            log('Order invalid. Order: %o', order);
        }

        if (!order.id || !validator.isInt(order.id.toString())) {
            log('order.id should be a number');
            return false;
        }

        if (!order.from) {
            log('order.location is required');
            return false;
        }

        const { latitude, longitude } = order.from;
        if (!latitude || !longitude) {
            log('latitude and longitude have to be present in location');
            return false;
        }

        if (!validator.isLatLong(`${latitude.toString()}, ${longitude.toString()}`)) {
            log('order.location is invalid');
            return false;
        }

        return true;
    }
}
