import * as debug from 'debug';
import { InterfaceOrder, InterfaceVehicle } from '../../common/entity';

const log = debug('app-vehicle-filter');

const filter = (order: InterfaceOrder, vehicles: InterfaceVehicle[]) => {
    const result = vehicles.filter((vehicle) => {
        const available = Object.entries(vehicle.features).filter((feature) => {
            return feature[1];
        }).map((feature) => feature[0]);
        log('Vehicle id: %o, available features: %o', vehicle.id, available);
        return Object.entries(order.features).every((feature) => {
            if (!feature[1]) {
                return true;
            }
            // TODO remove questionmarks
            const map = {
                animal: 'animal_grid',
                baby: 'child_seat',
                escort: 'escort',
                // rta: '?????',
                terminal: 'terminal',
                // vehicle: '???????',
                wifi: 'wifi',
            };

            const includes = available.includes(map[feature[0]]);
            if (!includes) {
                log('Vehicle id: %o does not have required feature(%o) to fulfill order', vehicle.id, feature[0]);
            }
            return available.includes(map[feature[0]]);
        });
    });

    return result;
};

export default filter;
