import { InterfaceStorage } from '@isolutions/distributor-lib';
import * as debug from 'debug';
import * as mysql from 'mysql';
import MYSQLConnectionError from '../../common/errors/mysql-connection-error';

export default class Storage implements InterfaceStorage {
    private connection;
    private host;
    private database;
    private table;
    private user;
    private password;
    private port;
    private queries = {
        CREATE_TABLE: 'CREATE TABLE IF NOT EXISTS {0} (order_id MEDIUMINT, vehicle_id MEDIUMINT)',
        INSERT_RESULT: 'INSERT INTO {0} VALUES ({1}, {2})',
    };
    private log;

    public constructor(config) {
        this.log = debug('app-mysql-storage');
        const { database, host, password, user, table, port } = config;
        this.database = database;
        this.host = host;
        this.password = password;
        this.port = port;
        this.user = user;
        this.table = table;
    }

    public async connect(): Promise<boolean> {
        this.log('Initializing server connection. Host: %o, Port: %o, Database: %o, Table: %o, User: %o',
            this.host, this.port, this.database, this.table, this.user,
        );
        try {
            this.connection = mysql.createConnection({
                database: this.database,
                host: this.host,
                password: this.password,
                port: this.port,
                table: this.table,
                user: this.user,
            });

            await new Promise((resolve, reject) => {
                this.connection.connect((error) => {
                    if (error) {
                        reject(new MYSQLConnectionError('Could not establish connection', error));
                    }

                    this.log('Connected');
                    resolve(true);
                });
            });

            await new Promise((resolve, reject) => {
                const query = this.format(this.queries.CREATE_TABLE, this.table);
                this.connection.query(query, (error) => {
                    if (error) {
                        reject(new MYSQLConnectionError('Error creating table', error));
                    }
                    resolve(true);
                });
                this.log('Table created');
            });

            this.log('Mysql server connected. Thread id: %o', this.connection.threadId);

            return true;
        } catch (error) {
            this.log(`${error.message}. Extra: %o`, error.extra);
            return false;
        }
    }

    public async store(orderId, entityId) {
        this.log('Storing result. Order Id: %o, Vehicle Id: %o', orderId, entityId);

        const query = this.format(this.queries.INSERT_RESULT, this.table, orderId, entityId);
        this.connection.query(query, (error) => {
            if (error) {
                this.log('Error creating table: %o', error);
                throw new Error(error);
            }
        });

        return true;
    }

    private format(template, ...rest) {
        return template.replace(/{(\d+)}/g, (match, count) => {
            return typeof rest[count] !== 'undefined' ? rest[count] : match;
        });
    }
}
