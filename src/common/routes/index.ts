export const LIST_ALL_ROUTE = '/';
export const LIST_AVAILABLE_ROUTE = '/available';
export const VIEW_VEHICLE_ROUTE = '/:id(\\d+)';
export const LOCK_VEHICLE_ROUTE = '/lock/:id';
export const UNLOCK_VEHICLE_ROUTE = '/unlock/:id';
export const NOT_FOUND = '*';
