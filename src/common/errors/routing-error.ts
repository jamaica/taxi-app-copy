export default class RoutingError extends Error {

    private code: number;

    constructor(message?: string, code?: number) {
        super(message); // 'Error' breaks prototype chain here
        Error.captureStackTrace(this, RoutingError);
        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
        this.code = code;
    }
}
