export default class MongoConnectionError extends Error {
    private extra: any;

    constructor(message: string, extra?: any) {
        super(message); // 'Error' breaks prototype chain here
        Error.captureStackTrace(this, MongoConnectionError);
        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
        this.extra = extra;
    }
}
