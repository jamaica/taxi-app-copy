import { InterfaceVehicleEntity } from '@isolutions/distributor-lib';

export interface InterfaceVehicleInfo {
  'manufacturer': string;
  'model': string;
  'color': string;
}

export interface InterfaceVehicleFeatures {
  'wifi': boolean;
  'terminal': boolean;
  'animal_grid': boolean;
  'child_seat': boolean;
  'mobile_charger': boolean;
  'escort': boolean;
  'mute': boolean;
  'test_mode': boolean; // TODO test cars
  'debug_mode': boolean; // TODO ignore
  'trusted_mode': boolean; // TODO Ignore
  'ignore_postponed': boolean; // TODO ignore
}

export interface InterfaceLocation {
  'latitude': number;
  'longitude': number;
}

export interface InterfaceDriver {
  'id': number;
  'firstname': string;
  'middlename': string;
  'lastname': string;
}

export interface InterfaceVehicle extends InterfaceVehicleEntity {
  'id': number;
  'controller_id': string;
  'license_tax': string;
  'hull_number': string;
  'phone_number': string;
  'info': InterfaceVehicleInfo;
  'features': InterfaceVehicleFeatures;
  'location': InterfaceLocation;
  'status': string;
  'driver': InterfaceDriver;
  'checkin_time': number;
}

export interface InterfaceCustomer {
  id?: number;
  name?: string;
  msid?: string;
}

export interface InterfaceOrderStatus {
  status?: string;
  is_terminal?: boolean;
}

export interface InterfaceOrderEvents {
  registered_at?: number;
  car_search_at?: number;
  car_found_at?: number;
  car_approved_at?: number;
  car_delivering_at?: number;
  car_delivered_at?: number;
  order_in_progress_at?: number;
  order_completed_at?: number;
  order_pending_payment_at?: number;
  order_paid_at?: number;
  order_closed_at?: number;
}

export interface InterfaceAddress {
  country?: string;
  settlement?: string  ;
  street?: string;
  building?: string;
}

export interface InterfaceObject {
  name?: string;
  type?: string;
}

export interface InterfaceOrderLocationDescription {
  address?: InterfaceAddress;
  object?: InterfaceObject;
}

export interface InterfaceOrderLocation {
  latitude: number;
  longitude: number;
  description?: InterfaceOrderLocationDescription;
  type?: string;
  features?: {};
}

export interface InterfaceOrderFeatures {
  rta?: boolean;
  animal?: boolean;
  baby?: boolean;
  escort?: boolean;
  terminal?: boolean;
  wifi?: boolean;
  vehicle?: string;
}

export interface InterfaceOrderOptions {
  developer?: boolean;
  ignored?: boolean;
}

/* TODO Possible dublicate with InterfaceVehicle */
export interface InterfaceOrderVehicle {
  license_tax?: string;
  title?: string;
  color?: string;
  type?: string;
  color_name?: string;
}

/* TODO Possible dublicate with InterfaceDriver */
export interface InterfaceOrderDriver {
  name?: string;
  msid?: string;
}

export interface InterfacePayments {
  created_at?: number;
  transited_at?: number;
  state?: string;
  amount?: number;
  method?: string;
  payment_identity?: string;
  payment_provider?: string;
}

export interface InterfaceOrder {
  id: number;
  created_at?: number;
  confirmation_expires_at?: number;
  customer?: InterfaceCustomer;
  status?: InterfaceOrderStatus;
  events?: InterfaceOrderEvents;
  from?: InterfaceOrderLocation;
  to?: InterfaceOrderLocation;
  arrive_at?: number;
  eta?: number;
  features?: InterfaceOrderFeatures;
  options?: InterfaceOrderOptions;
  comment?: string;
  vehicle?: InterfaceOrderVehicle;
  driver?: InterfaceOrderDriver;
  review?: string;
  amount?: number;
  payments?: InterfacePayments[];
}
