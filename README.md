### Order Distribution Services
Last update: 1/09/2018

#### TOC
1. Deploying collection
2. Deploying distributor
3. For development

#### 1. Deploying collection
 a) docker build --no-cache  -f ./Dockerfile --target collection-node .
 b) docker run -t -p 3000:3000 -d %CONTAINER_ID%
 

#### 2. Deploying distributor
 a) docker build --no-cache  -f ./Dockerfile --target distributor-node .
 b) docker run -t -p 3001:3001 -d %CONTAINER_ID%


#### 3. For development
 Use docker-compose.yml to setup osrm, mysql and rabbitmq external dependencies
 Update hostnames in Dockerfile to your docker network interface IP (
     Debian\Ubuntu: /sbin/ifconfig | grep -a1  docker | grep  -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -n1
  )