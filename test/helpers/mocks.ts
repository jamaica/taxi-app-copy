export const MockAMPQTransport = {
    getInstance: (mockedMethods) => {
        const mock = jest.fn(() => mockedMethods);
        return new mock();
    },
};

export const MockAMPQConnection = {
    getInstance: (mockedMethods) => {
        const mock = jest.fn(() => mockedMethods);
        return new mock();
    },
};

export const MockAMPQChannel = {
    getInstance: (mockedMethods) => {
        const mock = jest.fn(() => mockedMethods);
        return new mock();
    },
};
