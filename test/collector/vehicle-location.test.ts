import {Server, SocketIO} from 'mock-socket';
import VehicleLocation from '../../src/collection/mutators/vehicle-location';

const initializeServer = () => {
    const mockServer = new Server('http://localhost:8080');

    return mockServer;
};

const initializeClient = () => {
    const client = new SocketIO('http://localhost:8080');

    return client;
};

beforeEach(async () => {
    this.server = initializeServer();
    this.client = initializeClient();

    const params = {host: 'localhost', port: 8080, locationTimeout: 5, pruneTimeout: 5};
    this.sut = new VehicleLocation(params);

    this.sut.setClient(this.client);
    await this.sut.connect();
});

afterEach((done) => {
    this.sut.disconnect();
    this.server.stop(done);
});

it('updates state with location on location received from the server', async (done) => {
    setTimeout(async () => {
        this.server.emit('location', {entity: 2, data: {latitude: 456, longitude: 654}});
        this.server.emit('location', {entity: 1, data: {latitude: 123, longitude: 321}});

        const state = [
            {controller_id: 1, location: {latitude: 111, longitude: 222}},
            {controller_id: 2, location: {latitude: 333, longitude: 444}},
        ];

        const result = await this.sut.handle(state);

        expect(result).toMatchSnapshot();
        done();
    }, 100);
});

it('keeps state intact in case no locations available', async (done) => {
    setTimeout(async () => {
        const state = [{controller_id: 1}];

        const result = await this.sut.handle(state);

        expect(result).toEqual(state);
        done();
    }, 100);
});

it('purges outdated locations', (done) => {
    const sut = new VehicleLocation({host: 'localhost', port: 8080, locationTimeout: 0.001, pruneTimeout: 0.1});
    sut.setClient(this.client);
    sut.connect();
    this.server.emit('location', {entity: 1, timestamp: Date.now(), data: {latitude: 123, longitude: 321}});
    setTimeout(async () => {
        const state = [
            {controller_id: 1, timestamp: Date.now()},
        ];

        const result = await sut.handle(state);

        expect(result).toEqual(state);
        done();
    }, 100);
});
