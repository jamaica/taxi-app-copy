import filter from '../../../src/distributor/services/vehicle-filter';

const generateOrder = (features = {}) => {
    const order = {
        features: {
            animal: true,
            baby: true,
            escort: true,
           // rta: true,
            terminal: true,
           // vehicle: true,
            wifi: false,
            ...features,
        },
        id: 1,
    };

    return order;
};

const generateVehicle = (features = {}) => {
    const vehicle = {
        checkin_time: null,
        controller_id: null,
        driver: null,
        features: {
            animal_grid: true,
            child_seat: true,
            debug_mode: false,
            escort: true,
            ignore_postponed: false,
            mobile_charger: true,
            mute: true,
            terminal: true,
            test_mode: false,
            trusted_mode: false,
            wifi: true,
            ...features,
        },
        hull_number: null,
        id: 1,
        info: null,
        license_tax: null,
        location: null,
        phone_number: null,
        status: null,
    };

    return vehicle;
};

it('returns filters out vehicles with missing features', () => {
    const order = generateOrder();
    const vehicle = generateVehicle({ animal_grid: false });

    const result = filter(order, [vehicle]);

    expect(result).toHaveLength(0);
});

it('returns vehicles with necessary features', () => {
    const order = generateOrder();
    const vehicle = generateVehicle();

    const result = filter(order, [vehicle]);

    expect(result).toEqual([vehicle]);
});

it('does not remove vehicles on feature not needed', () => {
    const order = generateOrder({ baby: false });
    const vehicle = generateVehicle({ child_seat: false });

    const result = filter(order, [vehicle]);

    expect(result).toEqual([vehicle]);
});
