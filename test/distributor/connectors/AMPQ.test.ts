import AMPQ from '../../../src/distributor/connectors/AMPQ';
import { MockAMPQChannel, MockAMPQConnection, MockAMPQTransport } from '../../helpers/mocks';

test('bind sets nack and requeue false in case of malformed order', async () => {
    const message = { fields: { deliveryTag: 1 }, content: 'not-a-json' };
    const channel = MockAMPQChannel.getInstance({
        ack: jest.fn().mockReturnValue(''),
        assertQueue: jest.fn().mockReturnValue(''),
        consume: jest.fn().mockImplementation((queue, callback) => {
            callback(message);
        }),
        nack: jest.fn().mockImplementation((msq, allUp, requeue) => {
            expect(msq.content).toEqual('not-a-json');
            expect(requeue).toEqual(false);
        }),
        prefetch: jest.fn().mockReturnValue(''),
    });
    const connection = MockAMPQConnection.getInstance({
        createChannel: jest.fn().mockReturnValue(channel),
    });
    const transport = MockAMPQTransport.getInstance({
        connect: jest.fn().mockReturnValue(connection),
    });
    this.sut = new AMPQ(transport, { prefetch: 10, queue: 'test', url: 'example.com' });
    await this.sut.connect();

    this.sut.bind([]);
});
