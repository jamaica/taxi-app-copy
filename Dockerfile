## Base
FROM node:8 AS base-image

RUN         mkdir /app
WORKDIR     /app

COPY        ["package.json",    "package.json"]
COPY        ["./node_modules",  "./node_modules"]
COPY        ["./dist/",         "./dist"]


## Collector worker
## NOTE:
FROM base-image AS collection-node


ENV EXPRESS_HOSTNAME "0.0.0.0"
ENV EXPRESS_PORT "3000"

ENV SYNC_URL "http://sync.taxi5.by:8081/vehicles&key=token"

ENV UPDATE_INTERVAL "10"
ENV STATE_LIFESPAN "900"

ENV TRACKER_URL "172.17.0.1:8080"

ENV DEBUG app-*

RUN npm install uws --save

EXPOSE 3000
CMD     ["npm", "run", "collector" ]



## Distributor worker
## NOTE:
FROM base-image AS distributor-node


ENV RABBITMQ_PREFETCH "10"
ENV RABBITMQ_QUEUE_NAME "orders"
ENV RABBITMQ_URL "amqp://172.17.0.1"
ENV ASSERT_QUEUE "{\"durable\": true, \"ttl\": 3600}"

ENV OSRM_HOSTNAME "http://172.17.0.1"
ENV OSRM_PORT "5000"

ENV VEHICLE_COLLECTION_HOSTNAME "http://172.17.0.1"
ENV VEHICLE_COLLECTION_PORT "3000"

ENV DRIVER_CONFIRMATION_PORT "3001"
ENV DRIVER_CONFIRMATION_SOCKET_CONNECTION_RETRY_COUNT "20"
ENV DRIVER_CONFIRMATION_SOCKET_CONNECTION_RETRY_INTERVAL "1"
ENV DRIVER_CONFIRMATION_SOCKET_CONNECTION_ACK_TIMEOUT "10"

ENV MYSQL_DATABASE "orders"
ENV MYSQL_HOST "172.17.0.1"
ENV MYSQL_PASSWORD 'password'
ENV MYSQL_PORT '3306'
ENV MYSQL_DECISION_RESULT_TABLE "decision_result"
ENV MYSQL_USER "distributor"

ENV DEBUG app-*

CMD     ["npm", "run", "distributor" ]

